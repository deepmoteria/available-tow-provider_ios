//
//  subTypeCell.h
//  Doctor Express Provider
//
//  Created by My Mac on 7/15/15.
//  Copyright (c) 2015 Deep Gami. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface subTypeCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTypeName;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblMainType;
@property (weak, nonatomic) IBOutlet UILabel *lblSubType;

@end
