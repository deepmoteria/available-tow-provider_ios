//
//  LoginVC.m
//  UberNewDriver
//
//  Created by Deep Gami on 27/09/14.
//  Copyright (c) 2014 Deep Gami. All rights reserved.
//

#import "LoginVC.h"

#import <GooglePlus/GooglePlus.h>
#import "UIImageView+Download.h"
#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

static NSString *const kKeychainItemName = @"Google OAuth2 For gglplustest";
static NSString *const kClientID = @"645141992800-2s7emjqeos8is3j7sommqjktcu9sqkjt.apps.googleusercontent.com";
static NSString *const kClientSecret = @"";

@interface LoginVC ()
{
    AppDelegate *appDelegate;
    BOOL internet,isFacebookClicked;
    NSMutableArray *arrForCountry;
    NSMutableDictionary *dictparam;
    int reTrive;
    NSString * strEmail;
    NSString * strPassword;
    NSString * strLogin;
    NSMutableString * strSocialId;
}

@end

@implementation LoginVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark -
#pragma mark - View Life Cycle

@synthesize txtPassword,txtEmail;

- (void)viewDidLoad
{
    [super viewDidLoad];
    reTrive=0;
    //NSLog(@"fonts: %@", [UIFont familyNames]);
    isFacebookClicked=NO;
    dictparam=[[NSMutableDictionary alloc]init];
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    strEmail=[pref objectForKey:PREF_EMAIL];
    strPassword=[pref objectForKey:PREF_PASSWORD];
    strLogin=[pref objectForKey:PREF_LOGIN_BY];
    strSocialId=[pref objectForKey:PREF_SOCIAL_ID];
    
    strLogin = @"manual";
    
    internet=[APPDELEGATE connected];
    
    if(strEmail!=nil)
    {
        //[self getSignIn];
    }
    
    [self customFont];
    
    [self.btnSignIn setTitle:NSLocalizedString(@"SIGN IN", nil) forState:UIControlStateNormal];
    
    [self.btnForgotPsw setTitle:NSLocalizedString(@"FORGOT_PASSWORD", nil) forState:UIControlStateNormal];
    
    //[self.btnSignUp setTitle:NSLocalizedString(@"SIGN_UP", nil) forState:UIControlStateNormal];
    
    self.txtEmail.placeholder = NSLocalizedString(@"EMAIL", nil);
    self.txtPassword.placeholder = NSLocalizedString(@"PASSWORD", nil);
    
    self.lblCanada.text = NSLocalizedString(@"CANADA", nil);
    
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapGestureLogin:)];
    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:singleTapGestureRecognizer];
    
    //self.txtEmail.placeholder
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=YES;
    //[self.btnSignUp setTitle:NSLocalizedString(@"Sign In", nil) forState:UIControlStateNormal];
    FBSDKLoginManager *logout = [[FBSDKLoginManager alloc] init];
    [logout logOut];
}

- (void)viewDidAppear:(BOOL)animated
{
    //[self.btnSignUp setTitle:NSLocalizedString(@"Sign In", nil) forState:UIControlStateNormal];
}

-(void)customFont
{
    /*self.txtEmail.font=[UberStyleGuide fontRegular];
     self.txtPassword.font=[UberStyleGuide fontRegular];
     
     self.btnForgotPsw.titleLabel.font = [UberStyleGuide fontRegularBold];
     self.btnSignIn.titleLabel.font = [UberStyleGuide fontRegularBold];
     self.btnSignUp.titleLabel.font = [UberStyleGuide fontRegularBold];
     
     self.btnSignIn=[APPDELEGATE setBoldFontDiscriptor:self.btnSignIn];
     self.btnForgotPsw=[APPDELEGATE setBoldFontDiscriptor:self.btnForgotPsw];
     self.btnSignUp=[APPDELEGATE setBoldFontDiscriptor:self.btnSignUp];
     */
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)handleSingleTapGestureLogin:(UITapGestureRecognizer *)tapGestureRecognizer;
{
    [self.txtEmail resignFirstResponder];
    [self.txtPassword resignFirstResponder];
}


-(void)signIn
{
    if (device_token==nil || [device_token isEqualToString:@""] || [device_token isKindOfClass:[NSNull class]] || device_token.length < 1)
    {
        device_token=@"11111";
    }
    if (strEmail==nil)
    {
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        strEmail=[pref objectForKey:PREF_EMAIL];
        strPassword=[pref objectForKey:PREF_PASSWORD];
        strLogin=[pref objectForKey:PREF_LOGIN_BY];
        strSocialId=[pref objectForKey:PREF_SOCIAL_ID];
    }
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
    [dictparam setValue:device_token forKey:PARAM_DEVICE_TOKEN];
    [dictparam setValue:@"ios" forKey:PARAM_DEVICE_TYPE];
    [dictparam setValue:strEmail forKey:PARAM_EMAIL];
    [dictparam setValue:strLogin forKey:PARAM_LOGIN_BY];
    if (![strLogin isEqualToString:@"manual"])
    {
        [dictparam setValue:strSocialId forKey:PARAM_SOCIAL_ID];
    }
    else
    {
        [dictparam setValue:strPassword forKey:PARAM_PASSWORD];
    }
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    [afn getDataFromPath:FILE_LOGIN withParamData:dictparam withBlock:^(id response, NSError *error)
     {
         if (response)
         {
             response = [[UtilityClass sharedObject] dictionaryByReplacingNullsWithStrings:response];
             if([[response valueForKey:@"success"] intValue]==1)
             {
                 arrUser=response;
                 strForIosShare = [arrUser valueForKey:@"ios_provider_link"];
                 strForAndroidShare = [arrUser valueForKey:@"android_provider_link"];
                 NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                 [pref setValue:[response valueForKey:@"token"] forKey:PREF_USER_TOKEN];
                 [pref setValue:[response valueForKey:@"id"] forKey:PREF_USER_ID];
                 [pref setValue:[NSString stringWithFormat:@"%@",device_token] forKey:PREF_DEVICE_TOKEN];
                 [pref setValue:txtEmail.text forKey:PREF_EMAIL];
                 [pref setValue:txtPassword.text forKey:PREF_PASSWORD];
                 if(isFacebookClicked == NO)
                     [pref setValue:@"manual" forKey:PREF_LOGIN_BY];
                 else
                     [pref setValue:@"facebook" forKey:PREF_LOGIN_BY];
                 [pref setBool:YES forKey:PREF_IS_LOGIN];
                 [pref setValue:[response valueForKey:@"is_approved"] forKey:PREF_IS_APPROVED];
                 [pref synchronize];
                 
                 txtPassword.userInteractionEnabled=YES;
                 [APPDELEGATE hideLoadingView];
                 [APPDELEGATE showToastMessage:(NSLocalizedString(@"SIGING_SUCCESS", nil))];
                 isFacebookClicked = NO;
                 
                 [self performSegueWithIdentifier:@"seguetopickme" sender:self];
             }
             else
             {
                 NSString *str = [response valueForKey:@"error_code"];
                 if([str intValue] == 406)
                 {
                     [self performSegueWithIdentifier:SEGUE_UNWIND sender:nil];
                 }
                 else
                 {
                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                     [alert show];
                 }
             }
         }
         [APPDELEGATE hideLoadingView];
         NSLog(@"REGISTER RESPONSE --> %@",response);
     }];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark -
#pragma mark - Sign In

-(void)getSignIn
{
    [self.view endEditing:YES];
    if([APPDELEGATE connected])
    {
        if([strLogin isEqualToString:@"manual"])
        {
            if(strEmail.length<1)
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_EMAIL", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                [alert show];
            }
            else if (![[UtilityClass sharedObject]isValidEmailAddress:self.txtEmail.text])
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_VALID_EMAIL", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                [alert show];
            }
            else if (self.txtPassword.text.length<1)
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_PASSWORD", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                [self signIn];
            }
        }
        else
        {
            if(strEmail.length<1)
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_EMAIL", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                [alert show];
            }
            else if (![[UtilityClass sharedObject]isValidEmailAddress:strEmail])
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_VALID_EMAIL", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                [self signIn];
            }
        }
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"No Internet" message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark -
#pragma mark - Button Action

- (IBAction)onClickSignIn:(id)sender
{
    [txtEmail resignFirstResponder];
    [txtPassword resignFirstResponder];
    
    strEmail=self.txtEmail.text;
    strPassword=self.txtPassword.text;
    if(isFacebookClicked == YES)
        strLogin=@"facebook";
    else
        strLogin=@"manual";
    
    [self getSignIn];
}

- (IBAction)googleBtnPressed:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"google"];
    
    self.navigationController.navigationBarHidden=NO;
    
    NSString *scope = kGTLAuthScopePlusLogin;
    GTMOAuth2Authentication * auth = [GTMOAuth2ViewControllerTouch
                                      authForGoogleFromKeychainForName:kKeychainItemName
                                      clientID:kClientID
                                      clientSecret:kClientSecret];
    GTMOAuth2ViewControllerTouch *authController;
    authController = [[GTMOAuth2ViewControllerTouch alloc]
                      initWithScope:scope
                      clientID:kClientID
                      clientSecret:kClientSecret
                      keychainItemName:kKeychainItemName
                      delegate:self
                      finishedSelector:@selector(viewController:finishedWithAuth:error:)];
    [[self navigationController] pushViewController:authController animated:YES];
    [auth beginTokenFetchWithDelegate:self didFinishSelector:@selector(auth:finishedRefreshWithFetcher:error:)];
}

- (void)auth:(GTMOAuth2Authentication *)auth finishedRefreshWithFetcher:(GTMHTTPFetcher *)fetcher error:(NSError *)error
{
    [self viewController:nil finishedWithAuth:auth error:error];
    if (error != nil)
    {
        NSLog(@"self .auth :%@",self.auth);
        
        NSLog(@"Authentication Error %@", error.localizedDescription);
        
        self.auth=nil;
        return;
    }
    self.auth=auth;
}

- (void)viewController:(GTMOAuth2ViewControllerTouch *)viewController finishedWithAuth:(GTMOAuth2Authentication *)auth error:(NSError *)error
{
    if (error != nil)
    {
        //UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Could not login" message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        //[alert show];
        NSLog(@"Authentication Error %@", error.localizedDescription);
        self.auth=nil;
        self.txtEmail.text = @"";
        self.txtPassword.text = @"";
        self.txtPassword.userInteractionEnabled = YES;
        [[GPPSignIn sharedInstance] signOut];
        [[GPPSignIn sharedInstance] disconnect];
        return;
    }
    else
    {
        //[APPDELEGATE showLoadingWithTitle:@"Wait..."];
        self.auth=auth;
        auth.shouldAuthorizeAllRequests = YES;
        NSLog(@"login in");
        [self ForRetrive];
    }
}

-(void)ForRetrive
{
    GTLServicePlus* plusService = [[GTLServicePlus alloc] init];
    plusService.retryEnabled = YES;
    
    [plusService setAuthorizer:self.auth];
    
    GTLQueryPlus *query = [GTLQueryPlus queryForPeopleGetWithUserId:@"me"];
    
    [plusService executeQuery:query
            completionHandler:^(GTLServiceTicket *ticket,
                                GTLPlusPerson *person,
                                NSError *error)
     {
         if (error)
         {
             GTMLoggerError(@"Error: %@", error);
         }
         else
         {
             reTrive++;
             [APPDELEGATE hideLoadingView];
             NSString *description = [NSString stringWithFormat:
                                      @"%@\n%@\n%@\n%@ Birthdate :%@ %@ %@", person.displayName,
                                      person.aboutMe,person.emails,person.birthday,person.image,person.name,person.gender];
             
             NSLog(@"response :%@",description);
             NSDictionary *dict=person.JSON;
             NSLog(@"Dict :%@",[dict valueForKey:@"emails"]);
             self.txtPassword.userInteractionEnabled = NO;
             NSMutableArray *arr=[[NSMutableArray alloc]init];
             arr=[dict valueForKey:@"emails"];
             NSDictionary *dictMain=[arr objectAtIndex:0];
             NSLog(@"array  :%@",[dictMain valueForKey:@"value"]);
             self.txtEmail.text = [dictMain valueForKey:@"value"];
             [PREF setValue:txtEmail.text forKey:PREF_EMAIL];
             [PREF setValue:@"google" forKey:PREF_LOGIN_BY];
             [PREF setValue:[dict valueForKey:@"id"] forKey:PREF_SOCIAL_ID];
             [PREF setBool:YES forKey:PREF_IS_LOGIN];
             [PREF synchronize];
             
             NSLog(@"log for self auth :%@",self.auth);
             NSLog(@"new image :%@",person.image.url);
             if(reTrive==2)
             {
                 [appDelegate showLoadingWithTitle:NSLocalizedString(@"ALREADY_LOGIN", nil)];
                 [self getSignIn];
             }
         }
     }];
}

- (IBAction)facebookBtnPressed:(id)sender
{
    if ([APPDELEGATE connected])
    {
        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
        
        [loginManager
         logInWithReadPermissions: @[@"public_profile", @"email", @"user_friends"]
         fromViewController:self
         handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
         
         {
             if (error) {
                 NSLog(@"Process error");
             } else if (result.isCancelled){
                 NSLog(@"Cancelled");
             } else {
                 NSLog(@"Logged in");
                 [APPDELEGATE showLoadingWithTitle:@"Please wait"];
                 
                 if ([FBSDKAccessToken currentAccessToken])
                 {
                     FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                                   initWithGraphPath:@"me"
                                                   parameters:@{@"fields": @"first_name, last_name, picture.type(large), email, name, id, gender"}
                                                   HTTPMethod:@"GET"];
                     [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                                           id result,
                                                           NSError *error)
                      {
                          // Handle the result
                          [APPDELEGATE hideLoadingView];
                          isFacebookClicked=YES;
                          self.txtPassword.userInteractionEnabled=NO;
                          NSLog(@"FB Response ->%@",result);
                          self.txtEmail.text=[result valueForKey:@"email"];
                          [PREF setValue:[result valueForKey:@"email"] forKey:PREF_EMAIL];
                          [PREF setValue:@"facebook" forKey:PREF_LOGIN_BY];
                          [PREF setValue:[result valueForKey:@"id"] forKey:PREF_SOCIAL_ID];
                          [PREF setBool:YES forKey:PREF_IS_LOGIN];
                          [PREF synchronize];
                          [self getSignIn];
                      }];
                 }
             }
         }];
        
        [loginManager logOut];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"NO_INTERNET_TITLE", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self
                                           cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
        
    }
}

- (IBAction)forgotBtnPressed:(id)sender
{
    
}

- (IBAction)backBtnPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onClickBack:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark - TextField Delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    int y=0;
    if (textField==self.txtEmail)
        y=100;
    else if (textField==self.txtPassword)
        y=150;
    [self.scrLogin setContentOffset:CGPointMake(0, y) animated:YES];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField == self.txtPassword)
    {
        strLogin = @"manual";
    }
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==self.txtEmail)
    {
        if(txtPassword.userInteractionEnabled==NO)
        {
            [textField resignFirstResponder];
            [self.scrLogin setContentOffset:CGPointMake(0, 0) animated:YES];
        }
        else
            [self.txtPassword becomeFirstResponder];
    }
    else if (textField==self.txtPassword)
    {
        [textField resignFirstResponder];
        [self.scrLogin setContentOffset:CGPointMake(0, 0) animated:YES];
    }
    return YES;
}

@end